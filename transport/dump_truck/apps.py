from django.apps import AppConfig


class DumpTruckConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dump_truck'
