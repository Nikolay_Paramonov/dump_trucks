from django.views.generic import ListView

from .forms import DumpTruckChoiceField
from .models import DumpTruck


class DumpTruckListView(ListView):
    model = DumpTruck
    template_name = 'dump_truck/dump_truck_list.html'
    context_object_name = 'Trucks'

    def get_context_data(self, **kwargs):
        context = super(DumpTruckListView, self).get_context_data(**kwargs)
        context['Model'] = DumpTruckChoiceField()
        context['Model'].initial = self.request.GET
        return context

    def get_queryset(self):
        if self.request.GET.get('dump_trucks_models'):
            trucks = DumpTruck.objects.select_related('truck_model').filter(
                truck_model__name__exact=self.request.GET.get('dump_trucks_models')).required_fields()
        else:
            trucks = DumpTruck.objects.select_related('truck_model').required_fields()
        return trucks
