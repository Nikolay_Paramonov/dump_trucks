from django.urls import path

from . import views

app_name = 'dump_truck'

urlpatterns = [
    path('', views.DumpTruckListView.as_view(), name='dump_truck_list'),
]
