from django.db import models
from django.core.validators import MinValueValidator


class DumpTruckQuerySet(models.QuerySet):
    def required_fields(self):
        return self.values('board_number', 'truck_model__name', 'truck_model__max_payload',
                           'current_weight', 'overload')


class DumpTruckManager(models.Manager):
    def get_queryset(self):
        return DumpTruckQuerySet(self.model)

    def required_fields(self):
        return self.get_queryset().required_fields()


class DumpTruckModel(models.Model):
    name = models.CharField(
        unique=True,
        max_length=255,
        verbose_name='модель самосвала'
    )
    max_payload = models.PositiveIntegerField(
        validators=[
            MinValueValidator(0)
        ],
        verbose_name='максимальная грузоподъемность, Т'
    )

    class Meta:
        verbose_name = 'Модель самосвала'
        verbose_name_plural = 'Модели самосвалов'

    def __str__(self):
        return self.name

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        super(DumpTruckModel, self).save(force_insert=False, force_update=False, using=None, update_fields=None)
        for dump_truck in self.dumptruck_set.all():
            dump_truck.save()


class DumpTruck(models.Model):
    board_number = models.CharField(
        unique=True,
        max_length=50,
        verbose_name='бортовой номер'
    )

    truck_model = models.ForeignKey(
        DumpTruckModel,
        on_delete=models.CASCADE,
        verbose_name='модель самосвала'
    )

    current_weight = models.PositiveIntegerField(
        validators=[
            MinValueValidator(0)
        ],
        verbose_name='текущий вес, Т'
    )

    overload = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        blank=True,
        null=True,
        validators=[
            MinValueValidator(0)
        ],
        verbose_name='перегруз, %'
    )

    objects = DumpTruckManager()

    class Meta:
        verbose_name = 'Самосвал'
        verbose_name_plural = 'Самосвалы'

    def __str__(self):
        return f'{self.truck_model.name}, бортовой номер {self.board_number}'

    def save(
            self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        try:
            self.overload = 0 if self.current_weight < self.truck_model.max_payload else (
                                 self.current_weight * 100 / self.truck_model.max_payload) - 100
        except ZeroDivisionError:
            self.overload = 0
        super(DumpTruck, self).save(force_insert=False, force_update=False, using=None, update_fields=None)
