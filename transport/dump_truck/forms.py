from django import forms
from .models import DumpTruckModel


class DumpTruckChoiceField(forms.Form):

    dump_trucks_models = forms.ModelChoiceField(
        queryset=DumpTruckModel.objects.values_list('name', flat=True),
        empty_label='Все',
        label='Модель',
        required=False,
    )
