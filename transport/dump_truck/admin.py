from django.contrib import admin

from .models import DumpTruckModel, DumpTruck


class DumpTruckAdmin(admin.ModelAdmin):
    exclude = ('overload',)
    readonly_fields = ('overload',)


admin.site.register(DumpTruckModel)
admin.site.register(DumpTruck, DumpTruckAdmin)
